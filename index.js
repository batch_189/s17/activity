/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

//first function here:
let nameResult = prompt("Enter you Full Name");

function printName() {
  console.log("Hello, " + nameResult);
}
printName();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

//second function here:

let movie1 = "Venom";
let movie2 = "Happiness";
let movie3 = "Spy Family";
let movie4 = "Black";
let movie5 = "I Kill Giants";

function printMyFavoriteMovies() {
  console.log("1. " + movie1);
  console.log("2. " + movie2);
  console.log("3. " + movie3);
  console.log("4. " + movie4);
  console.log("5. " + movie5);
}
printMyFavoriteMovies();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/


// 68%
// 91%
// 78%
//third function here:

// let rate1 = "Venom";
// let rate2 = "The Batman";
// let rate3 = "Joker";
// let rate4 = "Official Competition"; 
// let rate5 = "I Kill Giants";

    let title1 = 'venom'
    let rating1 = '80%';

    function printFirstShowRating() {
        console.log('Ratings');
        console.log('1. ' + title1);
        console.log('Rotten Tomatoes Rating: ' + rating1);
    }
    printFirstShowRating();

    let title2 = 'The Batman'
    let rating2 = '85%';

    function printFirstShowRatingTwo() {
        console.log('2. ' + title2);
        console.log('Rotten Tomatoes Rating: ' + rating2);
    }
    printFirstShowRatingTwo();

    let title3 = 'Joker'
    let rating3 = '68%';

    function printFirstShowRatingThree() {
        console.log('3. ' + title3);
        console.log('Rotten Tomatoes Rating: ' + rating3);
    }
    printFirstShowRatingThree();

    let title4 = 'Official Competition'
    let rating4 = '91%';

    function printFirstShowRatingFour() {
        console.log('4. ' + title4);
        console.log('Rotten Tomatoes Rating: ' + rating4);
    }
    printFirstShowRatingFour();

    let title5 = 'I Kill Giants';
    let rating5 = '78%';

    function printFirstShowRatingFive() {
        console.log('5. ' + title5);
        console.log('Rotten Tomatoes Rating: ' + rating5);
    }
    printFirstShowRatingFive();

/*

        4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function printUsers() {
  alert("Hi! Please add the names of your friends.");
  let friend1 = prompt("Enter your first friend's name:");
  let friend2 = prompt("Enter your second friend's name:");
  let friend3 = prompt("Enter your third friend's name:");

  console.log("You are friends with:");
  console.log(friend1);
  console.log(friend2);
  console.log(friend3);
};

printFriends();

/*
console.log(friend1);
console.log(friend2);*/
